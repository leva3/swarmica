from django.contrib import admin

from mainapp.models import(
    Departament,
    Visitor,
    Book,
    VisitorBook,
)

admin.site.register(Departament)
admin.site.register(Visitor)
admin.site.register(Book)
admin.site.register(VisitorBook)
