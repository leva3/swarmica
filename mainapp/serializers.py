from rest_framework import serializers

from mainapp.exceptions import(
    BookNotFound,
)

from mainapp.models import(
    Departament,
    Visitor,
    Book,
    VisitorBook,
)

class DepartamentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Departament
        fields = (
            'id', 'name',
        )

class VisitorsWithTotalSerializer(serializers.ListSerializer):
    def to_representation(self, data):
        representation = super().to_representation(data)
        
        total_books = 0
        for item in representation:
            if isinstance(item, dict) and 'total_books' in item:
                total_books += item['total_books']

        representation.append({'total_books': total_books})
        return representation


class VisitorSerializer(serializers.ModelSerializer):
    total_books = serializers.SerializerMethodField()

    class Meta:
        model = Visitor
        fields = (
            'id', 'name', 
            'total_books',
        )
        list_serializer_class = VisitorsWithTotalSerializer

    def get_total_books(self, obj):
        return obj.visitor_books.count()


        



class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = (
            'id', 'name',
            'author',
            'year',
            'amount',
            'departament',
        )


class VisitorBookSerializer(serializers.ModelSerializer):
    class Meta:
        model = VisitorBook
        fields = (
            'id',
            'book',
            'visitor',
        )

    def create(self, validated_data):
        book = validated_data.get('book')
        visitor = validated_data.get('visitor')
        if book.amount == 0:
            raise BookNotFound()
        book.amount -= 1
        book.save()
        book_visitor = VisitorBook.objects.create(
            book=book,
            visitor=visitor
        )
        return book_visitor
    

class VisitorDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Visitor
        fields = (
            'id', 'name',
            
        )

    def to_representation(self, instance):
        return {
            'id': instance.id,
            'name': instance.name,
            'books': [BookSerializer(instance=visitor_book.book).data for visitor_book in instance.visitor_books.all()]
        }
