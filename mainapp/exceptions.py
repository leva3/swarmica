from rest_framework.exceptions import APIException

class BookNotFound(APIException):
    status_code = 404
    default_detail = 'Book with thos id not found'
    default_code = 'Book not found'
