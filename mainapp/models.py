from django.db import models

from django.db.models.signals import pre_delete
from django.dispatch import receiver


class Departament(models.Model):
    name = models.CharField(
        max_length=127, verbose_name='Название отдела'
    )

    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name = 'Отдел'
        verbose_name_plural = 'Отделы'


class Visitor(models.Model):
    name = models.CharField(
        max_length=127, verbose_name = 'Имя посетителя'
    )

    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name = 'Посетитель'
        verbose_name_plural = 'Посетители'

    @property
    def my_books(self):
        return self.visitor_books.all()


class Book(models.Model):
    departament = models.ForeignKey(
        Departament,
        on_delete=models.CASCADE,
        verbose_name='Отдел',
        related_name='books'
    )
    name = models.CharField(max_length=127, verbose_name='Название')
    author = models.CharField(max_length=127, verbose_name='Имя автора')
    year = models.PositiveIntegerField(verbose_name='Год выпуска', default=2020)
    amount = models.PositiveIntegerField(verbose_name='Кол-во книг', default=10)

    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name = 'Книга'
        verbose_name_plural = 'Книги'


class VisitorBook(models.Model):
    book = models.ForeignKey(
        Book, on_delete=models.CASCADE,
        verbose_name = 'Книга', related_name='visitor_books'
    )
    visitor = models.ForeignKey(
        Visitor, on_delete=models.CASCADE,
        verbose_name='Посетитель', related_name='visitor_books'
    )

    def __str__(self):
        return f'{self.book.name} - {self.visitor.name}'

    class Meta:
        verbose_name = 'Книга у поситителя'
        verbose_name_plural = 'Книги у поситителей'


@receiver(pre_delete, sender=VisitorBook)
def visitor_book_delete(sender, instance:VisitorBook, *args, **kwargs):
    instance.book.amount += 1
    instance.save()
    instance.book.save()
