from rest_framework.viewsets import(
    ModelViewSet,
    ReadOnlyModelViewSet,
)

from django.db.models import Count

from rest_framework.permissions import(
    AllowAny,
)

from rest_framework.response import(
    Response,
)

from mainapp.models import(
    Departament,
    Visitor,
    Book,
    VisitorBook,
)

from mainapp.serializers import(
    DepartamentSerializer,
    BookSerializer,
    VisitorSerializer,
    VisitorBookSerializer,
    VisitorDetailSerializer,
    VisitorsWithTotalSerializer,
)

from django_filters.rest_framework import(
    DjangoFilterBackend,
)


class DepartamentView(ModelViewSet):
    queryset = Departament.objects.all()
    serializer_class = DepartamentSerializer
    permission_classes = (
        AllowAny,
    )


class VisitorView(ModelViewSet):
    queryset = Visitor.objects.all()
    serializer_class = VisitorSerializer
    permission_classes = (
        AllowAny,
    )

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return VisitorDetailSerializer
        
        else: return VisitorSerializer


class BookView(ModelViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    permission_classes = (
        AllowAny,
    )
    filter_backends = (
        DjangoFilterBackend, 
    )

    filterset_fields = (
        'author',
        'year',
        'departament__name',
        'departament__id',
    )

    def get_queryset(self):
        available = self.request.query_params.get('available', None)
        if available is not None:
            if available == 'true':
                return Book.objects.filter(amount__gt=0)
            elif available == 'false':
                return Book.objects.filter(amount=0)
        return Book.objects.all()



class VisitorBookView(ModelViewSet):
    queryset = VisitorBook.objects.all()
    serializer_class = VisitorBookSerializer
    permission_classes = (
        AllowAny,
    )
