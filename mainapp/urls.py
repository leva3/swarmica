from rest_framework.routers import DefaultRouter as DR

from django.urls import path

from mainapp.views import(
    DepartamentView,
    BookView,
    VisitorBookView,
    VisitorView,
)


router = DR()

router.register('departament', DepartamentView, basename='departament')
router.register('book', BookView, basename='book')
router.register('visitor', VisitorView, basename='visitor')
router.register('visitor_book', VisitorBookView, basename='visitor_book')


urlpatterns = [

]


urlpatterns += router.urls
